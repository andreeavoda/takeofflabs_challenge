import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import static java.util.Arrays.sort;

public class SecondProblem {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
//stabilirea numarului de elemente si citirea sirului
        System.out.println("Introduceti numarul elementelor (N):");

        int n = s.nextInt();

        int arr[] = new int[n];

        System.out.println("Introduceti elementele:");

        for (int i = 0; i < n; i++) {//for reading array
            arr[i] = s.nextInt();
        }


//sortarea si afisarea sirului
        sort(arr);
        System.out.println("Sirul ordonat:");
        for (int i : arr) { //for printing array
            System.out.print(i + "   ");
        }


//introducerea valorilor matricei intr-un set pentru a extrage valorile distincte

        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < arr.length; i++) {
            set.add(arr[i]);
        }
        Integer[] subArr = new Integer[set.size()];
        set.toArray(subArr);

        System.out.println("\nValorile distincte ale sirului:");
        for (int i : subArr) { //for printing array
            System.out.print(i + "  ");
        }

        int subArr2 = 0;
        int aparitii = 0;

        for (int i = 0; i < subArr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (subArr[i] == arr[j])
                    aparitii++;
                if (aparitii > n / 2) {
                    System.out.println("Valoarea care apare de mai mult de N/2=" + n / 2 + " ori este " + subArr[i] + ".");
                    break;
                }

            }
            if (aparitii > n / 2)
                break;
            else
                aparitii = 0;
        }
        }

    }


