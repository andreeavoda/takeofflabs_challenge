// 1. Given an array of N distinct natural numbers, how many pairs of numbers sum up to a given number S?

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import static java.util.Arrays.sort;

public class FirstProblem {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);

        System.out.println("Enter the number of elements in the array:");

        int n = s.nextInt();
        int arr[] = new int[n];

        System.out.println("Enter the elements:");

        for (int i = 0; i < n; i++) {//for reading array
            arr[i] = s.nextInt();
        }

        System.out.println("Enter the sum S:");
        sort(arr);

        int sum = s.nextInt();

        int x = 0;

        for (int i = 0; i < n; i++) { //for printing array
            if (arr[i] > sum) {
                x = i;
                break;
            }
        }

        int[] subArr = Arrays.copyOfRange(arr, 0, x);

        Set<Integer> set = new HashSet<>();

        for (int i = 0; i < subArr.length; i++) {
            set.add(subArr[i]);
        }
        Integer[] subArr2 = new Integer[set.size()];
        set.toArray(subArr2);

        System.out.println("\nSorted and filtered array:");
        for (int i : subArr2) { //for printing array
            System.out.print(i + "  ");
        }

        int testSum = 0;

        int pairs = 0;

        for (int i = 0; i <= subArr2.length; i++) {

            for (int j = i + 1; j < subArr2.length; j++) {
                testSum = subArr2[j] + subArr2[i];
                if (testSum > sum)
                    break;
                if (testSum == sum)
                    pairs++;
            }
        }
        System.out.println("\nThe number of pairs that sum up to  S " + pairs);
    }


}