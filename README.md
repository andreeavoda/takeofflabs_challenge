The problems:

1. Given an array of N distinct natural numbers, how many pairs of numbers sum up to a given number S?
2. Given an array of N natural numbers, find the number that appears more than N/2 times.